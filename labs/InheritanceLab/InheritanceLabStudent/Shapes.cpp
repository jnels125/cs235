#include "Shapes.hpp"


/* RECTANGLE ***********************************************************/
/***********************************************************************/

void Rectangle::Setup( float width, float height, string unit )
{
    Shape::Setup( unit );
    m_width = width;
    m_height = height;
}

void Rectangle::DisplayArea()
{
    float area = CalculateArea();
    cout << area << " ft^" << m_unit << endl;
}

float Rectangle::CalculateArea()
{
    return m_width * m_height;
}

/* CIRCLE **************************************************************/
/***********************************************************************/

void Circle::Setup( float radius, string unit )
{
    Shape::Setup( unit );
    m_radius = radius;
}

void Circle::DisplayArea()
{
    float area = CalculateArea();
    cout << area << " ft^" << m_unit << endl;
}

float Circle::CalculateArea()
{
    return 3.14 * m_radius * m_radius;
}

/* SHAPE (parent class) FUNCTIONS **************************************/
/***********************************************************************/

void Shape::Setup( string unit )
{
    m_unit = unit;
}

void Shape::DisplayArea()
{
    cout << " " << m_unit << "^2";
}

float Shape::CalculateArea()
{
    return 0;
}
