#ifndef _PLAYERS_HPP
#define _PLAYERS_HPP

#include <cstdlib>
#include <string>
#include <iostream>
using namespace std;

class Character
{
    public:
    Character();
    Character( string name );

    string GetAction( int choice );
    int GetHP();
    int GetArmor();
    string GetName();
    void SetName( string name );
    int GetHitRoll();
    int GetDamageRoll();
    int Heal();
    void TakeDamage( int hitRoll, int damageRoll );
    void ChangeHP( int amount );

    protected:
    string m_name;
    int m_hp;       // health points
    int m_maxHp;
    int m_armor;    // armor
    int m_str;      // strength
    int m_healSlots;
    string m_weaponName;
};

class PlayerCharacter : public Character
{
    public:
    PlayerCharacter( string name );
    string GetAction();
    int GetDamageRoll();
};

class NonPlayerCharacter : public Character
{
    public:
    NonPlayerCharacter( string name );
    string GetAction();
    int GetDamageRoll();
};

#endif
