#ifndef _STORAGE_HPP
#define _STORAGE_HPP

#include <stdexcept>
#include <iomanip>
#include <iostream>
using namespace std;

#include "../Utilities/Menu.hpp"

template <typename T>
class Storage
{
    public:
    Storage();

    void Add( T newItem );
    void Display();

    bool IsFull();

    private:
    const int ARRAY_SIZE;
    int m_itemCount;
    T m_arr[20];
};

template <typename T>
Storage<T>::Storage()
    : ARRAY_SIZE( 20 )
{
    m_itemCount = 0;
}

template <typename T>
bool Storage<T>::IsFull()
{
    return ( m_itemCount == ARRAY_SIZE );
}

template <typename T>
void Storage <T>::Add( T newItem )
{
    if ( IsFull() )
    {
        throw runtime_error( "Array is full!" );
    }

    m_arr[ m_itemCount ] = newItem;
    m_itemCount++;
}

template <typename T>
void Storage<T>::Display()
{
    Menu::Header( "DISPLAY CONTENTS" );

    const int COL1 = 3;
    const int COL2 = 20;
    const int COL3 = 3;

    cout << right << setw( COL1 ) << "#"
        << setw( COL3 ) << " | " << left
        << left << setw( COL2 ) << "val"
        << right << setw( COL1 ) << "#"
        << setw( COL3 ) << " | "
        << left << setw( COL2 ) << "val"
        << right << setw( COL1 ) << "#"
        << setw( COL3 ) << " | "
        << left << setw( COL2 ) << "val"
        << endl;

    Menu::DrawHorizontalBar( 80, '-' );

    for ( int i = 0; i < m_itemCount; i++ )
    {
        if ( i != 0 && i % 3 == 0 )
        {
            cout << endl;
        }

        cout << left
        << setw( COL1 ) << i
        << setw( COL3 ) << " | "
        << setw( COL2 ) << m_arr[i];
    }

    cout << endl << endl;
}

#endif
