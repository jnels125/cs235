#ifndef _FILE_SYSTEM_THING
#define _FILE_SYSTEM_THING

#include <iostream>
#include <string>
using namespace std;

class FileSystemThing
{
    public:
    FileSystemThing();
    FileSystemThing( string name, string type );

    void    SetName( string name );
    string  GetName();

    string GetType();

    void Display();
    void Details();

    protected:
    string m_name;
    string m_type;
};

#endif
