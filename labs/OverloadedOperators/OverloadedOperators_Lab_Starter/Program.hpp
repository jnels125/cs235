#ifndef _PROGRAM
#define _PROGRAM

#include "CipherText.hpp"

class Program
{
    public:
    Program();

    void Run();

    private:
    CipherText m_cipherTexts[2];
    bool m_done;

    void Header();
    void Menu_SetText();
    void Menu_GetText();
    void Menu_GetOffset();
    void Menu_Display();
    void Menu_PlusOperator();
    void Menu_MinusOperator();
    void Menu_EquivalentOperator();
    void Menu_OutputStreamOperator();
    void Menu_InputStreamOperator();
    void Menu_SubscriptOperator();
    void Menu_Quit();
};

#endif
