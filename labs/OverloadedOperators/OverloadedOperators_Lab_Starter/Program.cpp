#include "Program.hpp"

#include "utilities/Menu.hpp"

Program::Program()
{
    m_cipherTexts[0].SetText( "Example text... 1 2 3" );
    m_cipherTexts[1].SetText( "Example text... 1 2 3" );
    m_done = false;
}

void Program::Run()
{
    while ( !m_done )
    {
        Menu::ClearScreen();
        Menu::Header( "CIPHER TEXT WIDGET" );

        Header();

        cout << endl << "OPERATIONS MENU" << endl;

        Menu::ShowCallbackMenuWithPrompt( {
            { "Set text",           bind( &Program::Menu_SetText, this ) },
            { "Get text",           bind( &Program::Menu_GetText, this ) },
            { "Get offset",         bind( &Program::Menu_GetOffset, this ) },
            { "Display",            bind( &Program::Menu_Display, this ) },
            { "+ operator",         bind( &Program::Menu_PlusOperator, this ) },
            { "- operator",         bind( &Program::Menu_MinusOperator, this ) },
            { "a == b?",            bind( &Program::Menu_EquivalentOperator, this ) },
            { "Output with <<",     bind( &Program::Menu_OutputStreamOperator, this ) },
            { "Input with >>",      bind( &Program::Menu_InputStreamOperator, this ) },
            { "Offset with []",     bind( &Program::Menu_SubscriptOperator, this ) },
            { " QUIT",     bind( &Program::Menu_Quit, this ) }
        } )();

        Menu::Pause();
    }
}

void Program::Header()
{
    cout << "m_cipherTexts[0]: " << m_cipherTexts[0].GetText() << " (" << m_cipherTexts[0].GetOffset() << ")" << endl;
    cout << "m_cipherTexts[1]: " << m_cipherTexts[1].GetText() << " (" << m_cipherTexts[1].GetOffset() << ")" << endl;
    cout << endl;
}

void Program::Menu_SetText()
{
    Menu::ClearScreen();
    Menu::Header( "Set Text" );
    Header();

    int index = Menu::GetValidChoice( 0, 1, "Enter which CipherText to modify (0, 1)" );

    string str = Menu::GetStringChoice( "Enter a non-encrypted string to store." );
    m_cipherTexts[index].SetText( str );
}

void Program::Menu_GetText()
{
    Menu::ClearScreen();
    Menu::Header( "Get Text" );
    Header();

    cout << "The m_cipherTexts[0] text currently is: " << m_cipherTexts[0].GetOffset() << endl;
    cout << "The m_cipherTexts[1] text currently is: " << m_cipherTexts[1].GetOffset() << endl;
}

void Program::Menu_GetOffset()
{
    Menu::ClearScreen();
    Menu::Header( "Get Offset" );
    Header();

    cout << "The m_cipherTexts[0] offset currently is: " << m_cipherTexts[0].GetOffset() << endl;
    cout << "The m_cipherTexts[1] offset currently is: " << m_cipherTexts[1].GetOffset() << endl;
}

void Program::Menu_Display()
{
    Menu::ClearScreen();
    Menu::Header( "Display" );
    Header();

    cout << "Calling m_cipherTexts[0].Display() ..." << endl;
    m_cipherTexts[0].Display();

    cout << "Calling m_cipherTexts[1].Display() ..." << endl;
    m_cipherTexts[1].Display();
}

void Program::Menu_PlusOperator()
{
    Menu::ClearScreen();
    Menu::Header( "+ Operator" );
    Header();

    int index = Menu::GetValidChoice( 0, 1, "Enter which CipherText to modify (0, 1)" );

    int offset = Menu::GetIntChoice( "Enter a number amount to add to the offset." );
    m_cipherTexts[index] = m_cipherTexts[index] + offset;

    cout << endl << "The CipherText string is now: " << m_cipherTexts[index].GetText() << endl;
}

void Program::Menu_MinusOperator()
{
    Menu::ClearScreen();
    Menu::Header( "- Operator" );
    Header();

    int index = Menu::GetValidChoice( 0, 1, "Enter which CipherText to modify (0, 1)" );

    int offset = Menu::GetIntChoice( "Enter a number amount to subtract the offset." );
    m_cipherTexts[index] = m_cipherTexts[index] - offset;

    cout << endl << "The CipherText string is now: " << m_cipherTexts[index].GetText() << endl;
}

void Program::Menu_EquivalentOperator()
{
    Menu::ClearScreen();
    Menu::Header( "== Operator" );
    Header();

    cout << "Are m_cipherTexts[0] and m_cipherTexts[1] equvalent?" << endl;

    if ( m_cipherTexts[0] == m_cipherTexts[1] )
    {
        cout << "They are equivalent." << endl;
    }
    else
    {
        cout << "They are NOT equivalent." << endl;
    }
}

void Program::Menu_OutputStreamOperator()
{
    Menu::ClearScreen();
    Menu::Header( "<< Operator" );
    Header();

    cout << "m_cipherTexts[0] is " << m_cipherTexts[0] << endl;
    cout << "m_cipherTexts[1] is " << m_cipherTexts[1] << endl;
}

void Program::Menu_InputStreamOperator()
{
    Menu::ClearScreen();
    Menu::Header( ">> Operator" );
    Header();

    int index = Menu::GetValidChoice( 0, 1, "Enter which CipherText to modify (0, 1)" );

    cout << "Enter one word to store in the CipherText: ";
    cin >> m_cipherTexts[index];

    cout << endl << "The CipherText string is now: " << m_cipherTexts[index].GetText() << endl;
}

void Program::Menu_SubscriptOperator()
{
    Menu::ClearScreen();
    Menu::Header( "[] Operator" );
    Header();

    int index = Menu::GetValidChoice( 0, 1, "Enter which CipherText to modify (0, 1)" );
    int offset = Menu::GetIntChoice( "Enter an offset amount to view (positive or negative is fine)." );

    CipherText modified( m_cipherTexts[index][offset] );
    cout << modified << endl;

    cout << endl << "Do you want to save this string in m_cipherTexts[" << index << "]?" << endl;
    string save = Menu::ShowStringMenuWithPrompt( { "yes", "no" } );

    if ( save == "yes" )
    {
        m_cipherTexts[index] = modified;
    }
}

void Program::Menu_Quit()
{
    Menu::ClearScreen();
    cout << "Goodbye." << endl;
    m_done = true;
}
