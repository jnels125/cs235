#include <iostream>
#include <string>
using namespace std;

#include "Program.hpp"

int main()
{
    Program program;
    program.Run();

    return 0;
}
