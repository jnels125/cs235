#ifndef _STUDENT_MANAGER_HPP
#define _STUDENT_MANAGER_HPP

#include "Student.hpp"

#include <vector>
using namespace std;

class Tester;

class StudentManager
{
    public:
    static void AddStudent( string studentName, int studentId );
    static void RemoveStudent( int studentId );
    static Student& GetStudent( int studentId );
    static void ListStudents();
    static int GetStudentCount();

    private:
    static int GetIndexOf( int studentId );
    static int GetIndexOf( string studentName );

    static vector<Student> students;

    friend class Tester;
};

#endif
