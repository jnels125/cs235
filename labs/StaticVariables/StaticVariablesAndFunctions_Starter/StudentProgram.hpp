#ifndef _STUDENT_PROGRAM_HPP
#define _STUDENT_PROGRAM_HPP

class StudentProgram
{
    public:
    void Run();

    private:
    void Menu_AddStudent();
    void Menu_RemoveStudent();
    void Menu_GetStudent();
    void Menu_ListStudents();
};

#endif
