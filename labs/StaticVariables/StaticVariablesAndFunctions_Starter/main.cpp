#include "tester/utilities/Menu.hpp"
#include "tester/StaticMemberLab_Tester.hpp"
#include "StudentProgram.hpp"

int main()
{
    Menu::Header( "STATIC MEMBER LAB" );
    int choice = Menu::ShowIntMenuWithPrompt( {
        "Run tests",
        "Run program",
        "Quit"
    } );

    if ( choice == 1 )
    {
        Tester tester;
        tester.Start();
    }
    else if ( choice == 2 )
    {
        StudentProgram program;
        program.Run();
    }

    return 0;
}
