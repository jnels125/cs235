#include "StudentManager.hpp"

#include <stdexcept>    // runtime_error
#include <iomanip>      // setw
#include <iostream>     // cin/cout
using namespace std;

// Required for static variable:
vector<Student> StudentManager::students;

void StudentManager::AddStudent( string studentName, int studentId )
{
    Student newStudent;
    newStudent.name = studentName;
    newStudent.id = studentId;
    students.push_back( newStudent );
}

void StudentManager::RemoveStudent( int studentId )
{
    if (GetIndexOf(studentId) == -1)
    {
        throw runtime_error( "Student ID not found!" );
    }
    else
    {
        students.erase( students.begin()+GetIndexOf(studentId) );
    }
}

Student& StudentManager::GetStudent( int studentId )
{
    for ( unsigned int i = 0; i < students.size(); i++ )
    {
        if (students[i].id == studentId)
        {
            return students[i];
        }
    }

    throw runtime_error( "Function not implemented!" );

}

void StudentManager::ListStudents()
{
    for (unsigned int i = 0; i < students.size(); i++)
    {
        cout << students[i].id << "\t" << students[i].name << endl;
    }
}

int StudentManager::GetStudentCount()
{
    return students.size();
}

int StudentManager::GetIndexOf( int studentId )
{
    for ( unsigned int i = 0; i < students.size(); i++ )
    {
        if (students[i].id == studentId)
        {
            return i;
        }
    }

    return -1;
}

int StudentManager::GetIndexOf( string studentName )
{
    for ( unsigned int i = 0; i < students.size(); i++ )
    {
        if (students[i].name == studentName)
        {
            return i;
        }
    }

    return -1;
}
