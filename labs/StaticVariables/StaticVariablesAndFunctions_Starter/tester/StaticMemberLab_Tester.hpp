#ifndef _TESTER_HPP
#define _TESTER_HPP

#include <iostream>
#include <string>
using namespace std;

#include "TesterBase.hpp"
#include "utilities/Menu.hpp"
#include "utilities/StringUtil.hpp"

#include "../StudentManager.hpp"

class Tester : public TesterBase
{
public:
	Tester()
	{
		AddTest(TestListItem("AddStudent()",
                bind(&Tester::AddStudent, this)));

		AddTest(TestListItem("RemoveStudent()",
                bind(&Tester::RemoveStudent, this)));

		AddTest(TestListItem("GetStudent()",
                bind(&Tester::GetStudent, this)));

		AddTest(TestListItem("GetStudentCount()",
                bind(&Tester::GetStudentCount, this)));

		AddTest(TestListItem("GetIndexOf()",
                bind(&Tester::GetIndexOf, this)));
	}

	virtual ~Tester() { }

private:
	int AddStudent();
	int RemoveStudent();
	int GetStudent();
	int GetStudentCount();
	int GetIndexOf();
};

int Tester::AddStudent()
{
    StartTestSet( "AddStudent", { "AddStudent()" } );

    int expectedId;
    string expectedName;

    int actualId;
    string actualName;

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Add a student, check index 0." );
        StudentManager::students.clear(); // Reset the students list
        StudentManager::AddStudent( "name", 123 );

        expectedId = 123;
        expectedName = "name";

        actualId = StudentManager::students[0].id;
        actualName = StudentManager::students[0].name;

        Set_ExpectedOutput  ( "id", expectedId );
        Set_ActualOutput    ( "id", actualId );
        Set_ExpectedOutput  ( "name", expectedName );
        Set_ActualOutput    ( "name", actualName );

        if ( actualId != expectedId )
        {
            TestFail( "ID doesn't match!" );
        }
        else if ( actualName != expectedName )
        {
            TestFail( "Name doesn't match!" );
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Add two students. Check index 1." );
        StudentManager::students.clear(); // Reset the students list
        StudentManager::AddStudent( "name1", 123 );
        StudentManager::AddStudent( "name2", 456 );

        // Check second student
        expectedId = 456;
        expectedName = "name2";

        actualId = StudentManager::students[1].id;
        actualName = StudentManager::students[1].name;

        Set_ExpectedOutput  ( "id", expectedId );
        Set_ActualOutput    ( "id", actualId );
        Set_ExpectedOutput  ( "name", expectedName );
        Set_ActualOutput    ( "name", actualName );

        if ( actualId != expectedId )
        {
            TestFail( "ID doesn't match!" );
        }
        else if ( actualName != expectedName )
        {
            TestFail( "Name doesn't match!" );
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/


    FinishTestSet();
    return TestResult();
}


int Tester::RemoveStudent()
{
    StartTestSet( "RemoveStudent", { "AddStudent()", "RemoveStudent()" } );

    int expectedId;
    string expectedName;

    int actualId;
    string actualName;

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Add a student, remove student, make sure list is empty." );
        StudentManager::students.clear(); // Reset the students list
        StudentManager::AddStudent( "name", 123 );
        StudentManager::RemoveStudent( 123 );

        int expectedSize = 0;
        int actualSize = StudentManager::students.size();

        Set_ExpectedOutput  ( "students.size()", expectedSize );
        Set_ActualOutput    ( "students.size()", actualSize );

        if ( actualSize != expectedSize )
        {
            TestFail( "Student size is wrong!" );
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/


	{ /* TEST BEGIN ************************************************************/
        StartTest( "Add students. Remove invalid ID. Make sure there's an exception." );
        StudentManager::students.clear(); // Reset the students list
        StudentManager::AddStudent( "name1", 123 );
        StudentManager::AddStudent( "name2", 456 );

        bool exceptionOccurred = false;

        try
        {
            StudentManager::RemoveStudent( 000 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        Set_ExpectedOutput  ( "exception occurred", true );
        Set_ActualOutput    ( "exception occurred", exceptionOccurred );

        if ( !exceptionOccurred )
        {
            TestFail( "Exception was not thrown during invalid removal!" );
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/


    FinishTestSet();
    return TestResult();
}


int Tester::GetStudent()
{
    StartTestSet( "GetStudent", { "AddStudent()", "GetStudent()" } );

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Add students. Try to search with valid ID." );
        StudentManager::students.clear(); // Reset the students list
        StudentManager::AddStudent( "name1", 111 );
        StudentManager::AddStudent( "name2", 112 );

        string expectedName = "name1";
        string actualName;

        bool exceptionThrown = false;
        try
        {
            Student& s = StudentManager::GetStudent( 111 );
            actualName = s.name;
        }
        catch( ... )
        {
            exceptionThrown = true;
        }

        Set_ExpectedOutput  ( "name", expectedName );
        Set_ActualOutput    ( "name", actualName );
        Set_ExpectedOutput  ( "exception thrown", false );
        Set_ActualOutput    ( "exception thrown", exceptionThrown );

        if ( actualName != expectedName )
        {
            TestFail( "Name doesn't match!" );
        }
        else if ( exceptionThrown )
        {
            TestFail( "Exception was thrown for a valid search!" );
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Add students. Try to search with invalid ID." );
        StudentManager::students.clear(); // Reset the students list
        StudentManager::AddStudent( "nameA", 111 );
        StudentManager::AddStudent( "nameB", 112 );

        bool exceptionThrown = false;
        try
        {
            Student& s = StudentManager::GetStudent( 000 );
        }
        catch( ... )
        {
            exceptionThrown = true;
        }

        Set_ExpectedOutput  ( "exception thrown", true );
        Set_ActualOutput    ( "exception thrown", exceptionThrown );

        if ( !exceptionThrown )
        {
            TestFail( "Exception wasn't thrown for an invalid search!" );
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/


    FinishTestSet();
    return TestResult();
}


int Tester::GetStudentCount()
{
    StartTestSet( "GetStudentCount", { "AddStudent()", "RemoveStudent()", "GetStudentCount()" } );

    int expectedCount;
    int actualCount;

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check empty student list, count should be 0." );
        StudentManager::students.clear(); // Reset the students list

        expectedCount = 0;
        actualCount = StudentManager::GetStudentCount();

        Set_ExpectedOutput  ( "GetStudentCount() result", expectedCount );
        Set_ActualOutput    ( "GetStudentCount() result", actualCount );

        if ( actualCount != expectedCount )
        {
            TestFail( "GetStudentCount() is incorrect!" );
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Add a student, count should be 1." );
        StudentManager::students.clear(); // Reset the students list
        StudentManager::AddStudent( "name", 123 );

        expectedCount = 1;
        actualCount = StudentManager::GetStudentCount();

        Set_ExpectedOutput  ( "GetStudentCount() result", expectedCount );
        Set_ActualOutput    ( "GetStudentCount() result", actualCount );

        if ( actualCount != expectedCount )
        {
            TestFail( "GetStudentCount() is incorrect!" );
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Add two students, count should be 2." );
        StudentManager::students.clear(); // Reset the students list
        StudentManager::AddStudent( "name1", 123 );
        StudentManager::AddStudent( "name2", 456 );

        expectedCount = 2;
        actualCount = StudentManager::GetStudentCount();

        Set_ExpectedOutput  ( "GetStudentCount() result", expectedCount );
        Set_ActualOutput    ( "GetStudentCount() result", actualCount );

        if ( actualCount != expectedCount )
        {
            TestFail( "GetStudentCount() is incorrect!" );
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Add two student, remove one, count should be 1." );
        StudentManager::students.clear(); // Reset the students list
        StudentManager::AddStudent( "name1", 123 );
        StudentManager::AddStudent( "name2", 456 );
        StudentManager::RemoveStudent( 123 );

        expectedCount = 1;
        actualCount = StudentManager::GetStudentCount();

        Set_ExpectedOutput  ( "GetStudentCount() result", expectedCount );
        Set_ActualOutput    ( "GetStudentCount() result", actualCount );

        if ( actualCount != expectedCount )
        {
            TestFail( "GetStudentCount() is incorrect!" );
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/


    FinishTestSet();
    return TestResult();
}



int Tester::GetIndexOf()
{
    StartTestSet( "GetIndexOf", { "AddStudent()", "GetIndexOf()" } );

    int expectedIndex;
    int actualIndex;

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Add students, get student by id." );
        StudentManager::students.clear(); // Reset the students list
        StudentManager::AddStudent( "name1", 123 );
        StudentManager::AddStudent( "name2", 456 );

        expectedIndex = 0;
        actualIndex = StudentManager::GetIndexOf( 123 );

        Set_ExpectedOutput  ( "index", expectedIndex );
        Set_ActualOutput    ( "index", actualIndex );

        if ( actualIndex != expectedIndex )
        {
            TestFail( "Index doesn't match!" );
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Add students, get student by id." );
        StudentManager::students.clear(); // Reset the students list
        StudentManager::AddStudent( "name1", 123 );
        StudentManager::AddStudent( "name2", 456 );

        expectedIndex = 1;
        actualIndex = StudentManager::GetIndexOf( 456 );

        Set_ExpectedOutput  ( "index", expectedIndex );
        Set_ActualOutput    ( "index", actualIndex );

        if ( actualIndex != expectedIndex )
        {
            TestFail( "Index doesn't match!" );
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Add students, get student by name." );
        StudentManager::students.clear(); // Reset the students list
        StudentManager::AddStudent( "name1", 123 );
        StudentManager::AddStudent( "name2", 456 );

        expectedIndex = 0;
        actualIndex = StudentManager::GetIndexOf( "name1" );

        Set_ExpectedOutput  ( "index", expectedIndex );
        Set_ActualOutput    ( "index", actualIndex );

        if ( actualIndex != expectedIndex )
        {
            TestFail( "Index doesn't match!" );
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Add students, get student by name." );
        StudentManager::students.clear(); // Reset the students list
        StudentManager::AddStudent( "name1", 123 );
        StudentManager::AddStudent( "name2", 456 );

        expectedIndex = 1;
        actualIndex = StudentManager::GetIndexOf( "name2" );

        Set_ExpectedOutput  ( "index", expectedIndex );
        Set_ActualOutput    ( "index", actualIndex );

        if ( actualIndex != expectedIndex )
        {
            TestFail( "Index doesn't match!" );
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Add students, search by invalid id" );
        StudentManager::students.clear(); // Reset the students list
        StudentManager::AddStudent( "name1", 123 );
        StudentManager::AddStudent( "name2", 456 );

        expectedIndex = -1;
        actualIndex = StudentManager::GetIndexOf( 000 );

        Set_ExpectedOutput  ( "index", expectedIndex );
        Set_ActualOutput    ( "index", actualIndex );

        if ( actualIndex != expectedIndex )
        {
            TestFail( "Index doesn't match!" );
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Add students, search by invalid name" );
        StudentManager::students.clear(); // Reset the students list
        StudentManager::AddStudent( "name1", 123 );
        StudentManager::AddStudent( "name2", 456 );

        expectedIndex = -1;
        actualIndex = StudentManager::GetIndexOf( "nobody" );

        Set_ExpectedOutput  ( "index", expectedIndex );
        Set_ActualOutput    ( "index", actualIndex );

        if ( actualIndex != expectedIndex )
        {
            TestFail( "Index doesn't match!" );
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/


    FinishTestSet();
    return TestResult();
}
#endif
