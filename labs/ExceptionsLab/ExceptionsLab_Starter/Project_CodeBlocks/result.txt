-2 /  -2  = 1                   
-2 /  -1  = 2                   
-2 /  0   = Cannot divide by zero!-2 /  1   = -2                  
-2 /  2   = -1                  
-1 /  -2  = 0.5                 
-1 /  -1  = 1                   
-1 /  0   = Cannot divide by zero!-1 /  1   = -1                  
-1 /  2   = -0.5                
0  /  -2  = -0                  
0  /  -1  = -0                  
0  /  0   = Cannot divide by zero!0  /  1   = 0                   
0  /  2   = 0                   
1  /  -2  = -0.5                
1  /  -1  = -1                  
1  /  0   = Cannot divide by zero!1  /  1   = 1                   
1  /  2   = 0.5                 
2  /  -2  = -1                  
2  /  -1  = -2                  
2  /  0   = Cannot divide by zero!2  /  1   = 2                   
2  /  2   = 1                   
