#include <iostream>
using namespace std;

#include "CritterManager.hpp"

int main()
{
    CritterManager crit;
    crit.Run();

    return 0;
}
