#include "Question.hpp"

#include "Exceptions/NotImplementedException.hpp"

#include <iostream>
using namespace std;

//! Default constructor - Initialize m_question and m_answer to empty strings.
Question::Question()
{
    m_question = "";
}

//! Parameterized constructor - Call the Setup function to initialize the question text.
Question::Question( string question )
{
    Setup( question );
}

//! Set up m_question and m_answer using the parameters.
void Question::Setup( string question )
{
    m_question = question;
}

//! Display the question text.
void Question::Display()
{
    cout << "-----------------------------" << endl;
    cout << m_question << endl;
}

/****************************************************************************************/
/* MultipleChoiceQuestion                                                               */
/****************************************************************************************/

void MultipleChoiceQuestion::Display()
{
    Display();
    for (int i = 0; i < 4; i++)
    {
        cout << m_options[i] << endl;
    }
}

bool MultipleChoiceQuestion::RunQuestion()
{
    int input;

    Display();
    cin >> input;

    cout << "Your answer: " << input << endl;
    if (input == m_answer)
    {
        cout << "Correct!";
        return true;
    }
    else
    {
        cout << "WRONG! The correct answer was " << m_answer << endl;
        return false;
    }

}

void MultipleChoiceQuestion::LoadQuestion(istream& in)
{
    string buffer;

    while (getline(in, buffer) && buffer != "QUESTION_END")
    {
        if (buffer == "QUESTION")
        {
            getline(in, m_question);
        }
        else if (buffer == "OPTION0")
        {
            getline(in, m_options[0]);
        }
        else if (buffer == "OPTION1")
        {
            getline(in, m_options[1]);
        }
        else if (buffer == "OPTION2")
        {
            getline(in, m_options[2]);
        }
        else if (buffer == "OPTION3")
        {
            getline(in, m_options[3]);
        }
        else if (buffer == "ANSWER")
        {
            getline(in, m_options[0]);
        }
    }
}
/****************************************************************************************/
/* TrueFalseQuestion                                                                    */
/****************************************************************************************/

void TrueFalseQuestion::Display()
{
    Question::Display();
    cout << "1. True" << endl << "2. False" << endl;
}

bool TrueFalseQuestion::RunQuestion()
{
    int input;
    Question::Display();
    cin >> input;
    cout << "Your answer: " << input << endl;
    if (input == 1)
    {
        if (m_answer == "True")
        {
            cout << "Correct!" << endl;
            return true;
        }
        else
        {
            cout << "WRONG! The correct answer is " << m_answer << endl;
            return false;
        }
    }
    else if (input == 2)
    {
        if (m_answer == "False")
        {
            cout << "Correct!" << endl;
            return true;
        }
        else
        {
            cout << "WRONG! The correct answer is " << m_answer << endl;
            return false;
        }
    }
    else
    {
        cout << "Invalid choice!";
    }
}

void TrueFalseQuestion::LoadQuestion(istream& in)
{
    string buffer;
    while (getline (in, buffer) && buffer != "QUESTION_END")
    {
        if (buffer == "QUESTION")
        {
            getline (in, m_question);
        }
        else if (buffer == "ANSWER")
        {
            in >> m_answer;
        }
    }
}
/****************************************************************************************/
/* FillInTheBlankQuestion                                                               */
/****************************************************************************************/

void FillInTheBlank::Display()
{
    Question::Display();
}

bool FillInTheBlank::RunQuestion()
{
    string input;
    Display();
    getline(cin, input);

    if (input == m_answer)
    {
        cout << "Correct!";
        return true;
    }
    else
    {
        cout << "WRONG! The correct answer is " << m_answer << endl;
        return false;
    }

}

void FillInTheBlank::LoadQuestion ( istream& in )
{
    string buffer;
    while (getline(in, buffer) && buffer != "QUESTION_END")
    {
        if (buffer == "QUESTION")
        {
            getline(in, m_question);
        }
        else if (buffer == "ANSWER")
        {
            in >> m_answer;
        }
    }
}
