#include "QuizzerApp.hpp"
#include "QuestionManager.hpp"

#include <iostream>
#include <string>
using namespace std;

QuizzerApp::QuizzerApp()
{
    m_totalAnswered = 0;
    m_totalCorrect = 0;
}

QuizzerApp::~QuizzerApp()
{
    QuestionManager::CleanupQuestions();
}

void QuizzerApp::Run()
{
    QuestionManager::LoadQuestions( "questions.txt" );

    while ( !QuestionManager::IsEmpty() )
    {
        Question* ptrQuestion = QuestionManager::GetNextQuestion();
        bool isCorrect = ptrQuestion->RunQuestion();

        if ( isCorrect )
        {
            m_totalCorrect++;
        }

        m_totalAnswered++;
    }

    float score = ( float(m_totalCorrect) / float(m_totalAnswered) ) * 100;

    cout << "-----------------------------" << endl;
    cout << endl << "QUIZ RESULT" << endl;
    cout << "Total correct:     " << m_totalCorrect << endl;
    cout << "Total questions:   " << m_totalAnswered  << endl;
    cout << "SCORE:             " << score << "%" << endl;
}
