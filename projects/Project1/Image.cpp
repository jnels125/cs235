#include "Image.hpp"

#include <fstream>
#include <iostream>
#include <exception>
#include <string>
using namespace std;

PpmImage::PpmImage()
{
    m_pixels = new Pixel*[IMAGE_WIDTH];

    for ( int i = 0; i < IMAGE_WIDTH; i++ )
    {
        m_pixels[i] = new Pixel[IMAGE_HEIGHT];
    }
}

PpmImage::~PpmImage()
{
    for ( int i = 0; i < IMAGE_WIDTH; i++ )
    {
        delete [] m_pixels[i];
    }

    delete [] m_pixels;
}

void PpmImage::LoadImage( const string& filename )
{
    cout << "Loading image from \"" << filename << "\"...";

    ifstream input;

    input.open (filename);

    if ( input.fail() )
    {
        throw runtime_error( "Method not implemented!" );
    }

    int intBuffer;
    string strBuffer;

    getline( input, strBuffer ); // Skip "P3"
    getline( input, strBuffer ); // Skip comment

    input >> intBuffer; // Skip width
    input >> intBuffer; // Skip height

    input >> m_colorDepth;

    int x = 0;
    int y = 0;

    while ( input >> m_pixels[x][y].r
                  >> m_pixels[x][y].g
                  >> m_pixels[x][y].b )
    {
        x++;
        if (x == IMAGE_WIDTH)
        {
            y++;
            x = 0;
        }
    }


    cout << "SUCCESS" << endl;
}

void PpmImage::SaveImage( const string& filename )
{
    ofstream output ( filename );

    cout << "Saving image to \"" << filename << "\"...";

    if ( output.fail() )
    {
        throw runtime_error( "Method not implemented!" );
    }

    output << "P3" << endl;
    output << "# Comment" << endl;
    output << IMAGE_WIDTH << " " << IMAGE_HEIGHT << endl;
    output << m_colorDepth << endl;

    for ( int y = 0; y < IMAGE_HEIGHT ; y ++ )
    {
        for ( int x = 0; x < IMAGE_WIDTH ; x ++ )
        {
            output
                << m_pixels [ x ][ y ]. r << endl
                << m_pixels [ x ][ y ]. g << endl
                << m_pixels [ x ][ y ]. b << endl ;
        }
    }
        cout << "SUCCESS" << endl;
}

void PpmImage::ApplyFilter1()
{
    for ( int y = 0; y < IMAGE_HEIGHT; y++ )
    {
        for ( int x = 0; x < IMAGE_WIDTH; x++ )
        {
            int y2 = IMAGE_HEIGHT - 1 - y;
            int x2 = IMAGE_WIDTH - 1 - x;

            m_pixels[x][y].r = m_pixels[x2][y2].r;
            m_pixels[x][y].g = m_pixels[x2][y2].g;
            m_pixels[x][y].b = m_pixels[x2][y2].b;
        }
    }
}

void PpmImage::ApplyFilter2()
{
    for ( int y = 0; y < IMAGE_HEIGHT; y++ )
    {
        for ( int x = 0; x < IMAGE_WIDTH; x++ )
        {
            int r =  m_pixels[x][y].r;
            int g =  m_pixels[x][y].g;
            int b =  m_pixels[x][y].b;

             m_pixels[x][y].r = g;
             m_pixels[x][y].g = b;
             m_pixels[x][y].b = r;
        }
    }
}

void PpmImage::ApplyFilter3()
{
    for ( int y = 0; y < IMAGE_HEIGHT; y++ )
    {
        for ( int x = 0; x < IMAGE_WIDTH; x++ )
        {
            int y2 = x;
            int x2 = y;

            m_pixels[x][y].r = m_pixels[x2][y2].r;
            m_pixels[x][y].g = m_pixels[x2][y2].g;
            m_pixels[x][y].b = m_pixels[x2][y2].b;
        }
    }
}


void PpmImage::ApplyFilter4()
{
    for ( int y = 0; y < IMAGE_HEIGHT; y++ )
    {
        for ( int x = 0; x < IMAGE_WIDTH; x++ )
        {
            int r = (rand() % 255);
            int g = (rand() % 255);
            int b = (rand() % 255);

            m_pixels[x][y].r = r;
            m_pixels[x][y].g = g;
            m_pixels[x][y].b = b;
        }
    }
}
